import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import React from 'react';
import Header from './components/Header';
import './App.css';

import HomePage from './routes/HomePage';
import ErrorBoundary from './ErrorBoundary';
import Movies from './routes/Movies';
import Series from './routes/Series';
import Footer from './components/Footer';


const App = () => (
  <Router>
    <Header />
    <ErrorBoundary>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/movies" component={Movies} />
        <Route exact path="/series" component={Series} />
      </Switch>
    </ErrorBoundary>
    <Footer />
  </Router>
);

export default App;
