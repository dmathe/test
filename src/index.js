import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

/* eslint-disable no-console */
if (process.env.NODE_ENV === 'production') { console.log('Welcome to production !'); }
if (process.env.NODE_ENV === 'development') { console.log('Welcome to development !'); }

ReactDOM.render(<App />, document.getElementById('root'));
