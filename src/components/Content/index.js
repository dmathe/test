import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 65vh;
`;
const Title = styled.div`
  display: flex;
  background: darkgrey;
  width: 100%;
  height: 10%;
  padding: auto 10vw;

`;
const Body = styled.div`
  display: flex;
  heigh: 90%;
  padding: 2em;
`;

const TextTitle = styled.span`
  color: white;
  font-size: 2em;
  margin: auto;
`;

const Button = styled.div`
  color: white;
  font-size: 4em;
  background-color: black;
  display: flex;
  flex-flow: center;
  justify-content: center;
`;

const Card = styled.a`
  height: 300px;
  width: 220px;
  display: flex;
  flex-flow: column;
  justify-content: center;
  background-color: black;
  margin: 20px;
  position: relative;
`;

const Legend = styled.div`
  color: white;
  font-size: 1em;
  position: absolute;
  bottom: 0;
  background: white;
  color: black;
  width: 100%;
  padding: 5px;
`;

export const TitleComponent = () => <Title><TextTitle>Popular Titles</TextTitle></Title>;


const Content = () => (
  <Wrapper>
    <TitleComponent />
    <Body>
      <Card href="/movies">
        <Button>
          Movies
        </Button>
        <Legend>
          Popular Movies
        </Legend>
      </Card>

      <Card href="/series">
        <Button>
          Series
        </Button>
        <Legend>
          Popular Series
        </Legend>
      </Card>
    </Body>
  </Wrapper>
);

export default Content;
