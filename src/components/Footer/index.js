import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import facebook from '../../assets/social/facebook-white.svg';
import twitter from '../../assets/social/twitter-white.svg';
import instagram from '../../assets/social/instagram-white.svg';

const Wrapper = styled.div`
  position: fixed;
  bottom: 0;
  height: 25vh;
  background-color: #3c3c3c;
  width: 100%;
  display: flex;
  flex-flow: column;
  justify-content: space-around;
  padding: 0 3vh;
`;

const Links = styled.div`
  display: flex;
  flex-flow: row;
`;

const LinkWrapper = styled.button`
  color: white;
  margin: 0 10px 0 0;
`;

const Copyright = styled.span`
  color: white;
`;

const SocialNetworks = styled.div`
  display: flex;
  flex-flow: row;
`;


const Svg = styled.img`
  width: 45px;
  height: 45px;
`;

const Buttons = styled.div``;


const links = ['Home', 'Terms and conditions', 'Privacy Policy', 'Collection Statement', 'Help', 'ManageAccount'];

const LinkComponent = ({ txt }) => (<LinkWrapper>{txt}</LinkWrapper>);

LinkComponent.propTypes = {
  txt: PropTypes.string.isRequired,
};

const Footer = () => (
  <Wrapper>
    <Links>
      {links && links.map((l) => (
        <LinkComponent key={`link-${l}`} txt={l} />
      ))}
    </Links>
    <Copyright>
      Copyright 2015 DEMO Streaming All rights Reserved.
    </Copyright>
    <SocialNetworks>
      <Svg alt="facebook" src={facebook} />
      <Svg alt="twitter" src={twitter} />
      <Svg alt="instragram" src={instagram} />
    </SocialNetworks>
    <Buttons />
  </Wrapper>
);

export default Footer;
