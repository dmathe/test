import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  background-color: blue;
  width: 100%;
  height: 10vh;
  display: flex;
  justify-content: space-around;
`;

const Title = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
const TitleText = styled.span`
  color: white;
  font-weight: bold;
  font-size: 3em;
`;
const LogInButton = styled.button`
  width: 8vw;
  height: 5vh;
  margin: auto 0;
`;
const FreeTrialButton = styled.button`
  background-color: black;
  width: 13vw;
  height: 5vh;
  margin: auto 0;
`;
const TextButton = styled.span`
  color: white;
`;

const Buttons = styled.div`
  display: flex;
`;

const Header = () => (
  <Wrapper>
    <Title><TitleText>DEMO Streaming</TitleText></Title>
    <Buttons>
      <LogInButton><TextButton>Log in</TextButton></LogInButton>
      <FreeTrialButton><TextButton>Start your free trial</TextButton></FreeTrialButton>
    </Buttons>
  </Wrapper>
);

export default Header;
