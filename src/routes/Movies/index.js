import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { TitleComponent } from '../../components/Content';
import sample from '../../feed/sample.json';

const MoviesWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-flow: row wrap;
  max-height: 55vh;
  overflow-y: scroll;
  padding: 5vh 0;
`;

const MovieCardWrapper = styled.div`
  width: 10vw;
  height: 15vh;
  margin: 2em;
`;

const Image = styled.img`
  width: 100%;
  height: 90%;
`;

const Title = styled.span`
  color: black;
`;

const MovieCard = ({ data }) => (
  <MovieCardWrapper>
    <Image src={data.images['Poster Art'].url} />
    <Title>
      {data.title}
    </Title>
  </MovieCardWrapper>
);

const Movies = () => {
  const [movies, setMovies] = useState([]);
  useEffect(() => {
    if (sample && sample.entries) {
      const moviesFiltered = sample.entries.filter((s) => s.programType === 'movie');
      const moviesRelease2010 = moviesFiltered.filter((s) => s.releaseYear >= 2010);
      const myMovies = moviesRelease2010.sort();
      setMovies(myMovies);
    }
  }, []);

  return movies ? (
    <>
      <TitleComponent />
      <MoviesWrapper>
        {movies && movies.map((m) => (
          <MovieCard key={`movie-${m.title}`} data={m} />
        ))}
      </MoviesWrapper>
    </>
  ) : 'Loading ...';
};

export default Movies;
