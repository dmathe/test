Initial start:
- npm install
- npm start

Lancer les tests:
- npm test

Si j'avais eu plus de temps:

- le code plitting pour separer style et composants react + lisibilite
- proptypes sur chaques composants
- tests sur chaques composants
- meilleur naming de composants
- utilisation de useMemo pour eviter calcul redondant sur les filters dans /movies
- amelioration du css
- finalisation de la page /series
- peut etre mis en place une meilleur architecture base sur des contexts.
- utilisation de fetch pour GET la donnee