const merge = require('webpack-merge');
const Dotenv = require('dotenv-webpack');
const WebpackDashboard = require('webpack-dashboard/plugin');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'cheap-module-eval-source-map',
  plugins: [new WebpackDashboard(), new Dotenv({ path: './.env.dev' })],
  devServer: {
    publicPath: '/',
    historyApiFallback: true,
    compress: true,
    port: 9000,
  },
});
