const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: { app: './src/index.js' },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        include: path.resolve(__dirname, 'src'),
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        options: { presets: ['@babel/env'] },
      },
      {
        test: /\.css$/,
        include: path.resolve(__dirname, 'src'),
        use: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        include: path.resolve(__dirname, 'src'),
        use: [
          'url-loader?limit=10000',
          'img-loader',
        ],
      },
    ],
  },
  plugins: [new MiniCssExtractPlugin(), new CleanWebpackPlugin(), new HtmlWebpackPlugin({
    // inject: false,
    title: 'Test',
    template: 'public/index.html',
    hash: true,
    filename: 'index.html',
    appMountId: 'root',
  })],
  resolve: { extensions: ['*', '.js', '.jsx'], symlinks: false },
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: '[name].bundle.[hash].js',
    chunkFilename: '[id].bundle.[hash].js',
    publicPath: '/',
  },
};
